# Default Haskell compilation
ccmd = ghc -threaded --make

# Default directory for auxiliary stuff
cfgdir = $(shell find . -type d -name 'aux' -printf "%P\n")

# The name of the configuration file 
cfgfile = chorgram.config

# Paths to HKC, petrify, and to the directory for tests
hkcpath := $(shell find . -type d -name hknt-1.0 -printf "%P\n")/bin
petripath := $(shell find . -type d -name petrify -printf "%P\n")/bin
experimentspath = $(shell find . -nowarn -type d -name experiments -maxdepth 1 -printf "%P\n")

# Trying to discriminate between Linux and Mac OS
os := $(shell uname -s)

# Path to executable commands
binpath = "$$HOME/bin/chorgram"

all:
	$(MAKE) gmc_hs &&\
	$(MAKE) wb_hs &&\
	$(MAKE) ws_hs &&\
	$(MAKE) wf_hs &&\
	$(MAKE) buildgc_hs &&\
	$(MAKE) PomsetSemantics_hs &&\
	$(MAKE) viewsystem_hs &&\
	$(MAKE) gc2pom_hs &&\
	$(MAKE) project_hs &&\
	$(MAKE) pom2gc_hs &&\
	$(MAKE) gc2fsa_hs &&\
	$(MAKE) gc2dot_hs &&\
	$(MAKE) gc2gml_hs &&\
	$(MAKE) cg_hs &&\
	$(MAKE) ptps_hs &&\
	$(MAKE) tsof_hs


setup:
	@if test -e experiments; then echo ">>> The directory experiments is already there. Nothing to be done."; else make -C experiments; echo ">>> directory experiments created"; fi
	make config
	make parser
	make all

config:
	@echo "experiments\t"$(experimentspath) > $(cfgdir)/$(cfgfile)
	$(info .)
	@echo "hkc\t"$(hkcpath) >> $(cfgdir)/$(cfgfile)
	$(info ..)
	@echo "petrify\t"$(petripath) >> $(cfgdir)/$(cfgfile)
	$(info ...)
	@echo "gmc\t./gmc" >> $(cfgdir)/$(cfgfile)
	$(info ....)
	@echo "bg\t./buildgc" >> $(cfgdir)/$(cfgfile)
	$(info .....)
	@echo "dot\taux/dot.cfg" >> $(cfgdir)/$(cfgfile)
	$(info >>> config file created $(cfgdir)/$(cfgfile))
	$(info ......)

parser: GCGrammar.y SystemGrammar.y SystemtoTikz.y GCtoLatex.y
	happy -a -i  GCGrammar.y -o GCParser.hs && $(ccmd) GCParser.hs
	happy -a -i  SystemGrammar.y -o SystemParser.hs && $(ccmd) SystemParser.hs

gmc_hs: gmc.hs SystemParser.hs FSA.hs CFSM.hs TS.hs Representability.hs Misc.hs DotStuff.hs BranchingProperty.hs PetrifyBridge.hs
	$(ccmd) $<

PomsetSemantics_hs: PomsetSemantics.hs CFSM.hs SyntacticGlobalChoreographies.hs Misc.hs DotStuff.hs
	$(ccmd) $<

buildgc_hs: buildgc.hs PetriNet.hs Misc.hs GlobalGraph.hs
	$(ccmd) $<

PetriNet_hs: PetriNet.hs Misc.hs
	$(ccmd) $<

viewsystem_hs: viewsystem.hs SystemParser.hs
	$(ccmd) $<

gc2pom_hs: gc2pom.hs Misc.hs GCParser.hs PomsetSemantics.hs
	$(ccmd) $<

pom2gc_hs: pom2gc.hs Misc.hs PomsetSemantics.hs SyntacticGlobalChoreographies.hs DotStuff.hs
	$(ccmd) $<

gc2fsa_hs: gc2fsa.hs Misc.hs GCParser.hs CFSM.hs FSA.hs SyntacticGlobalChoreographies.hs
	$(ccmd) $<

project_hs: project.hs Misc.hs GCParser.hs CFSM.hs FSA.hs SyntacticGlobalChoreographies.hs
	$(ccmd) $<

gc2dot_hs: gc2dot.hs Misc.hs PomsetSemantics.hs SyntacticGlobalChoreographies.hs DotStuff.hs GCParser.hs
	$(ccmd) $<

gc2gml_hs: gc2gml.hs Misc.hs SyntacticGlobalChoreographies.hs GCParser.hs
	$(ccmd) $<

wb_hs: wb.hs Misc.hs GCParser.hs WellFormedness.hs
	$(ccmd) $<

ws_hs: ws.hs Misc.hs GCParser.hs WellFormedness.hs Misc.hs DotStuff.hs
	$(ccmd) $<

wf_hs: wf.hs Misc.hs GCParser.hs WellFormedness.hs Misc.hs DotStuff.hs
	$(ccmd) $<

chorgram_hs: chorgram.hs Misc.hs
	$(ccmd) $<

ptps_hs: ptps.hs GCParser.hs Misc.hs
	$(ccmd) $<

tsof_hs: tsof.hs GCParser.hs Misc.hs
	$(ccmd) $<

showconfig:
	clear
	@echo cfgdir=$(cfgdir)
	@echo hkcpath=$(hkcpath)
	@echo petripath=$(petripath)
	@echo experimentspath=$(experimentspath)

hp:
	@if test -e $(hkcpath)/hkc; then echo ">>> The binary of hkc is already there. Nothing to be done."; else make -C $(hkcpath); echo ">>> hkc compiled"; fi
	@if test -e $(hkcpath)/hkc$(os); then echo ">>> The link to hkc is already there. Nothing to be done."; else (cd $(hkcpath); ln -s hkc hkc$(os)) ; echo ">>> link to petrify added"; fi
	@if test -e $(petripath)/petrify$(os); then echo ">>> The link to petrify is already there. Nothing to be done."; else (cd $(petripath); ln -s petrify petrify$(os)); fi

clean:
	@rm -f *~ *.o *.hi SystemParser.* GCParser.* KGparser.* $(cfgfile) *.info *.log
	@mkdir ___
	@find . -nowarn -type f -executable -name "*.*" -maxdepth 1 -exec mv {} ___ \;
	@find . -nowarn -type f -executable -maxdepth 1 -exec rm {} \;
	@mv ___/* .
	@rmdir ___
	$(info >>> cleaning done.)

bin:
	$(info >>> Linking executables)
	@if test ! -d $(binpath); then mkdir $(binpath); fi
	$(info >>> CLI and auxiliary stuff commans done)
	@for f in chorgram hkc petrify; do if test ! -e "$(binpath)/$$f"; then ln -s "$$PWD/$$f" "$(binpath)/$$f"; fi; done
	@for f in; do if test ! -e "$(binpath)/$$f"; then ln -s "$$PWD/$$f" "$(binpath)/$$f"; fi; done
	$(info >>> Commands for semantics done)
	@for f in buildgc gc2pom gcunfold gmc handleND pom2gc project wb wf ws; do if test ! -e "$(binpath)/$$f"; then ln -s "$$PWD/$$f" "$(binpath)/$$f"; fi; done
	$(info >>> Commands for syntax and formats done)
	@for f in cfsm2dot gc2dot gc2gml gc2latex sgg ptps tsof viewsystem; do if test ! -e "$(binpath)/$$f"; then ln -s "$$PWD/$$f" "$(binpath)/$$f"; fi; done
